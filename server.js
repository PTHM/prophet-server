var password = "CqKVprY3RfK34eMBIXlcJvp3Qsv8i4kiohEK91G8cMlZujwkAFyUDb2SYtB0OvDEf02yt4kwhJZfrBQrZtM5wEfVYvnDLhFBxZpXIOZh5MT0fA3M0JdjjpGpRcLP5mgfd0ZunLO0TvV6TEOhw6QsD6vDLYgCVxPvGEVwBVCAEofeYdJUUt7vq62Pj30kwbD9N4I0cfSn4iiShjiheO8i4TbYW8uGKG3Sua2fKh7FRkdyWURG0AZH0H9FMZPxrGFZdlBRGqx3P7QKoMaiQLYdwWjAyrzFYA7twfp7XslCz9wjFaTvoHZtm6Y3UPWiyzt1WqQaz78X6VEZ54e406YDYYMkDKJgD5ZaWaRcAjiY5lrldpUOHyz06VWRPGog4sW5EpCHqWtd0T5I2aAL";
var escape = require('escape-html');
var fs = require('fs');
var colors = require('colors');
var async = require('async');

//Redis Init
var redis = require("redis"),
    mcReceiveClient = redis.createClient(),
    mcSendClient = redis.createClient(),
    laraReceiveClient = redis.createClient(),
    prophetSendClient = redis.createClient();

mcReceiveClient.auth(password);
mcSendClient.auth(password);
laraReceiveClient.auth(password);
prophetSendClient.auth(password);
console.log('Redis Connected!');


//MySQL Connection
var mysql      = require('mysql');
var connection = mysql.createConnection({
    host     : '127.0.0.1',
    user     : 'root',
    password : 'BHNETmysql123'
});
connection.connect(function(err) {
    connection.query('use bhnet');
    console.log('MySQL Connected!');
});


//HTTPS Server
var cfg = {
    ssl: true,
    port: 8080,
    ssl_ca: '/root/sslfiles/sub.class1.server.ca.pem',
    ssl_key: '/root/sslfiles/ssl.key',
    ssl_cert: '/root/sslfiles/ssl.crt'
};

var httpServ = ( cfg.ssl ) ? require('https') : require('http');
var WebSocketServer = require('ws').Server
var app = null;
//Dummy request processing
var processRequest = function( req, res ) {
    res.writeHead(200);
    res.end("All glory to WebSockets!\n");
};

if ( cfg.ssl ) {
    app = httpServ.createServer({
        ca:   fs.readFileSync(cfg.ssl_ca),
        key: fs.readFileSync( cfg.ssl_key ),
        cert: fs.readFileSync( cfg.ssl_cert ),
        passphrase: 'Catface8878',
    }, processRequest ).listen( cfg.port );

} else {
    app = httpServ.createServer( processRequest ).listen( cfg.port );
}

// passing or reference to web server so WS would knew port and SSL capabilities
var wss = new WebSocketServer( { server: app } );

createWebChat();

//Init Ends Here ----------------------------------------------------------------------------

//WebSockets
wss.on('connection', function(ws) {
    prophetSendClient.lrange("prophet:serverlist", 0, -1, function(error, items){
        var servers = [];
        items.forEach(function(item){
            servers.push(JSON.parse(item));
        });
        var message = {
            type: 'channellist',
            data: servers
        }
        try{
            ws.send(JSON.stringify(message));
        }catch(err){
            console.log(err.red)
        }
    });
    ws.on('message', function(msg) {
        var message = JSON.parse(msg);
        if(message.type = 'requestchat'){
            console.log('Requested Chat for channel '+message.data.channel);
            prophetSendClient.lrange("prophet:chatlog:"+message.data.channel, 0, -1, function(error, items){
                items.reverse().forEach(function(item){
                    try{
                        ws.send(item);
                    } catch(err) {
                        console.log(err.red);
                    }
                });
            });
        }
        console.log('Websocket client sent: '+msg)
    });
});
wss.broadcast = function(data) {
    for(var i in this.clients){
        try{
            this.clients[i].send(data);
        } catch(err) {
            console.log(err.red);
        }
    }
};


//Redis Pub/Sub
mcReceiveClient.on("subscribe", function (channel, count) {
    console.log('Subscribed to: ' + channel.cyan)
});

mcReceiveClient.on("message", function (channel, message) {
    console.log("Message on " + channel.magenta + " " + message.green);
    var message =  JSON.parse(message);
    message.source = "mc";
    if(mcCommands[message.type]){
        mcCommands[message.type].call(this, message);
    } else {
        console.log('Unexpected Command! '+message.type.red);
    }
});
mcReceiveClient.subscribe("prophet:web");

var mcCommands = {
    'link' : function(message){
        connection.query("UPDATE user_preferences SET `mcusername`='"+message.data.name+"', `mc_uuid`='"+message.data.uuid+"', `linked`=1 WHERE `mc_link_code`='"+message.data.code+"'", function(err, rows, fields) {
            if (err) throw err;

            if(rows.affectedRows > 0){
                var returnMessage = "§2Your account is now successfully linked with Boxhead Networks!";
            } else {
                var returnMessage = "§4An error occured, please try again."
            }

            var data = {
                'name' : message.data.name,
                'message' : returnMessage
            };
            mcSend(message.server, 'message', data, 'Web');
            console.log(message.data.name + ' linked with code '.green + message.data.code);
        });
    },
    'chat' : function(message){
        wss.broadcast(JSON.stringify(message));
        logChat(JSON.stringify(message),message.server)
    },
    'status' : function(message){
        wss.broadcast(JSON.stringify(message))
    },
    'auth' : function(message){
        var server = {
            name : message.server,
            channel: message.data.channel,
        }
        prophetSendClient.lrem("prophet:serverlist", 0, JSON.stringify(server));
        prophetSendClient.lpush("prophet:serverlist", JSON.stringify(server));
        prophetSendClient.set("prophet:server:"+message.server, message.data.channel);
    },
    'error' : function(message){
        connection.query("INSERT INTO error_log VALUES(null, '"+message.data.message+"','"+message.data.file+"','"+message.data.method+"','"+message.data.stacktrace+"','"+message.data.log+"',1,'"+message.server+"',null,null)",function(err, rows, fields){
            if (err) throw err;
        });
    }
};

//Laravel Receive
laraReceiveClient.on("subscribe", function (channel, count) {
    console.log('Subscribed to: ' + channel.cyan)
});
laraReceiveClient.on("message", function (channel, message) {
    console.log("Message on " + channel.magenta + " " + message.blue);
    var message =  JSON.parse(message);
    message.source = "web";
    if(laraCommands[message.type]){
        laraCommands[message.type].call(this, message);
    } else {
        console.log('Unexpected Command! '+message.type.red);
    }
});
laraReceiveClient.subscribe("prophet:larareceive");

var laraCommands = {
  'chat' : function(message){
    mcSend(message.to, 'chat', message.data, 'web')
    wss.broadcast(JSON.stringify(message));
    logChat(JSON.stringify(message), message.server)
  },
  'status' : function(message){
    message.source = "web";
    wss.broadcast(JSON.stringify(message))
  }
};

//Utility Functions

function mcSend(to, type, data, from){
    var messageObject = {
        type: type,
        data: data,
        server: from,
    }
    mcSendClient.publish("prophet:mc:"+to, JSON.stringify(messageObject));
}

function logChat(message, server){
    prophetSendClient.lpush("prophet:chatlog:"+server, message);
    prophetSendClient.ltrim("prophet:chatlog:"+server, 0, 30);
}

function createWebChat(){
    var server = {
        name : 'Web Chat',
        channel: 'web',
    }
    prophetSendClient.lrem("prophet:serverlist", 0, JSON.stringify(server));
    prophetSendClient.lpush("prophet:serverlist", JSON.stringify(server));
}